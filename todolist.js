


document.addEventListener("DOMContentLoaded", () =>{
    let body = document.querySelector("body");
    let colorValue = 255;
    
    const colorChangeInterval = setInterval (function () {
        if (colorValue < 200){
            clearInterval(colorChangeInterval);
        }
        colorValue -= 5;
        body.style.backgroundColor = `rgb(${colorValue}, ${colorValue}, ${colorValue})`
    }, 400);
    
    });

    setTimeout(() => {
        document.getElementById("app-heading").style = "color:white";
    }, 5000);
    


showtask();
let addtaskinput = document.getElementById("addtaskinput");
let addtaskbtn = document.getElementById("addtaskbtn");


//Add task

addtaskbtn.addEventListener("click", function(){
    addtaskinputval = addtaskinput.value;
    if(addtaskinputval.trim()!=0){
        let webtask = localStorage.getItem("localtask");
        if(webtask == null){
            taskObj = [];
        } 
        else{
            taskObj = JSON.parse(webtask);
        }
        taskObj.push({'task_name':addtaskinputval, 'completeStatus':false});
        
		//localStorage
        localStorage.setItem("localtask", JSON.stringify(taskObj));
      
    }
    addtaskinput.value=''; // to make blank addTask
    showtask();
})

// showtask
function showtask(){
    let webtask = localStorage.getItem("localtask");
    if(webtask == null){
        taskObj = [];
    }
    else{
        taskObj = JSON.parse(webtask);
    }
    let page = '';
    let addedtasklist = document.getElementById("addedtasklist");
    taskObj.forEach((item, index) => {

        if(item.completeStatus==true){
            taskCompleteValue = `<td class="completed">${item.task_name}</td>`;
        }else{
            taskCompleteValue = `<td>${item.task_name}</td>`;
        }

        // add task and Deleteall
        page += `<tr> 
                    <th scope="row">${index+1}</th>
                    ${taskCompleteValue}
                    <td><button type="button" onclick="edittask(${index})" class="text-primary"><i class="fa fa-edit"></i>Edit</button></td>
                    <td><button type="button" onclick="deleteitem(${index})" class="text-danger"><i class="fa fa-trash"></i>Delete</button></td>
                </tr>`;
    });
    addedtasklist.innerHTML = page;
}

// edittask
function edittask(index){
    let saveindex = document.getElementById("saveindex");
    let addtaskbtn = document.getElementById("addtaskbtn");
    let savetaskbtn = document.getElementById("savetaskbtn");
    saveindex.value = index;
    let webtask = localStorage.getItem("localtask");
    let taskObj = JSON.parse(webtask); 
    
    addtaskinput.value = taskObj[index]['task_name'];
    addtaskbtn.style.display="none";
    savetaskbtn.style.display="block";
}

// savetask
let savetaskbtn = document.getElementById("savetaskbtn");
savetaskbtn.addEventListener("click", function(){
    let addtaskbtn = document.getElementById("addtaskbtn");
    let webtask = localStorage.getItem("localtask");
    let taskObj = JSON.parse(webtask); 
    let saveindex = document.getElementById("saveindex").value;
    
    for (keys in taskObj[saveindex]) {
        if(keys == 'task_name'){
            taskObj[saveindex].task_name = addtaskinput.value;
        }
      }
 //to change savetask to addtask btn.
    savetaskbtn.style.display="none";
    addtaskbtn.style.display="block";
    localStorage.setItem("localtask", JSON.stringify(taskObj));
    addtaskinput.value=''; // to make blank addTask
    showtask();
})
// to deleteTask
function deleteitem(index){
    let webtask = localStorage.getItem("localtask");
    let taskObj = JSON.parse(webtask); 
    taskObj.splice(index, 1) // to delete 1 item by click
    localStorage.setItem("localtask", JSON.stringify(taskObj)); // to delete from localStorage too
    addtaskinput.value=''; // to make blank addTask
    showtask();
}

//deleteall
let deleteallbtn = document.getElementById("deleteallbtn");
deleteallbtn.addEventListener("click", function(){
    let savetaskbtn = document.getElementById("savetaskbtn");// to make a blank input task are when click on Deleteall
    let addtaskbtn = document.getElementById("addtaskbtn"); // to make a blank input task are when click on Deleteall
    let webtask = localStorage.getItem("localtask");
    let taskObj = JSON.parse(webtask); 
    if(taskObj !=0){ // tocheck
        taskObj = [];
    }
    else{
        taskOb =JSON.parse(webtask);
        taskObj = [];
    }
    savetaskbtn.style.display="none" // to make a blank input task are when click on Deleteall
    addtaskbtn.style.display="block"; // to make a blank input task are when click on Deleteall
    localStorage.setItem("localtask", JSON.stringify(taskObj));
    addtaskinput.value=''; // to make blank addTask
    showtask();

});


// validation


const email = document.getElementById("email")
const message = document.getElementById("message")
const form = document.getElementById("connect-form")
const submit = document.getElementById("connect-fom")

form.addEventListener("submit", function (e) {
    
    if (email.value.match(/\w+@\w+\.\w+/)) {
        email.classList.toggle("valid")}

        else{
            email.classList.toggle("invalid")
        
        email.nextElementSibling.innerText = "Incorrect Email"
        e.preventDefault()
    }
    if (message.value.length > 10){
        message.classList.toggle("valid")}
        else{
            message.classList.toggle("invalid")
        message.nextElementSibling.innerText = "Message must be 10 or more characters"
        e.preventDefault()
    }
    console.log("FORM SUBMIT")
    e.preventDefault()

});
//////////////////
